# ELS Integration For Laravel

Provides API support for ELS Vouchering v1.6 ([electronicline](http://www.electronicline.com))

## About


## Installation

Add the repository to your composer respository section:
```json
"repositories": [
   {
      "type": "vcs",
      "url": "git@bitbucket.org:saythanks/electronicline.git"
   }
],
```

Require the package via composer:
```sh
composer require saythanks/electronicline
```

## Configuration

Add your environment variables:
```

```

The defaults are set in `config/electronicline.php`. Publish the config to copy the file to your own config:
```sh
php artisan vendor:publish --provider="SayThanks\Electronicline\ElectroniclineServiceProvider"
```




## Usage
