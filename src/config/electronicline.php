<?php

return

    /*
    |--------------------------------------------------------------------------
    | TDS API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [
        'host' => env('ELS_HOST', 'https://elsqa.toget.me/'),
        'private_key' => env('ELS_PRIVATE'),
        'short_code' => env('ELS_PROGRAMME_SHORTCODE'),
        'programme_id' => env('ELS_PROGRAMME_ID'),
    ];