<?php

namespace SayThanks\Electronicline;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SayHi\Switchfox\Exceptions\AuthorizationException;
use SayHi\Switchfox\Exceptions\GenericException;
use SayThanks\Electronicline\Exceptions\InvalidConfigException;

class Electronicline
{

    public string $host;
    public string $shortcode;
    public int $programmeId;
    private string $privateKey;


    public function __construct()
    {
        $this->host = config('electronicline.host');
        $this->privateKey = config('electronicline.private_key');
        $this->shortcode = config('electronicline.short_code');
        $this->programmeId = config('electronicline.programme_id');

        $this->validateConfig();
    }


    /**
     * @throws InvalidConfigException
     */
    protected function validateConfig(): void
    {
        if(!$this->host)
        {
            throw new InvalidConfigException("Unable to find ELS Host in your config!");
        }
        if(!$this->privateKey)
        {
            throw new InvalidConfigException("Unable to find ELS Private Key in your config!");
        }
        if(!$this->shortcode)
        {
            throw new InvalidConfigException("Unable to find ELS Programme Shortcode in your config!");
        }
        if(!$this->programmeId)
        {
            throw new InvalidConfigException("Unable to find ELS Programme ID in your config!");
        }
    }

    public function getUser(string $email = null, string $msisdn = null, string $externalId = null, string $programmeShortCode = null, string $nickname = null, bool $loginToken = false)
    {
        if(is_null($email) && is_null($msisdn) && is_null($externalId))
        {
            throw new GenericException('ELS-Api: At least one of Email/Msisdn/ExternalId is required for GetUser');
        }

        $shortcode = $programmeShortCode ?? $this->shortcode;

        $url = "/ElP.Application.Platform/Platform/api/Users/GetUser/{$shortcode}";
        $data = [
            'email' => $email,
            'mobileNumber' => $msisdn,
            'externalId' => $externalId,
            'nickname' => $nickname,
            'getLoginToken' => $loginToken,
        ];

        $response = $this->elsApi()->post($this->host . $url, $data);
        $this->checkForErrors($response);

        return $response->json();
    }

    public function createUser()
    {
        // ToDo
    }

    public function getPartners($programmesShortcode = null)
    {
        $shortcode = $programmesShortcode ?? $this->shortcode;
        $url = "/ElP.Application.Platform/Platform/api/Programmes/GetPartners/{$shortcode}";
        $data = [];

        $response = $this->elsApi()->get($this->host . $url, $data);
        $this->checkForErrors($response);

        return $response->json();
    }

    public function getPartnerVouchers(string $partnerShortcode, $voucherTypeId = 0, $voucherValueTypeId = 0, $topUp = true, $giftEnabled = true, $onlyActiveVouchers = true)
    {
        $url = "/ElP.Application.Voucher/Voucher/api/VoucherInfo/GetPartnerVouchers";
        $data = [
            'shortCode' => $partnerShortcode,
            '"additionalVoucherParameters":' =>
                [
                    "voucherTypeId" => $voucherTypeId,
                    "voucherValueTypeId" => $voucherValueTypeId,
                    "topUp" => $topUp,
                    "giftEnabled" => $giftEnabled,
                    "onlyActiveVouchers" => $onlyActiveVouchers,
                ],
        ];

        $response = $this->elsApi()->post($this->host . $url, $data);
        $this->checkForErrors($response);

        return $response->json();
    }

    public function getProgrammeVouchers(string $programmeShortcode, $voucherTypeId = 0, $voucherValueTypeId = 0, $topUp = true, $giftEnabled = true, $onlyActiveVouchers = true)
    {
        $url = "/ElP.Application.Voucher/Voucher/api/VoucherInfo/GetProgrammeVouchers";
        $data = [
            'shortCode' => $programmeShortcode,
            'additionalVoucherParameters' =>
                [
                    "voucherTypeId" => $voucherTypeId,
                    "voucherValueTypeId" => $voucherValueTypeId,
                    "topUp" => $topUp,
                    "giftEnabled" => $giftEnabled,
                    "onlyActiveVouchers" => $onlyActiveVouchers,
                ],
        ];

        $response = $this->elsApi()->post($this->host . $url, $data);
        $this->checkForErrors($response);

        return $response->json();
    }

    public function purchaseVoucher($partnerShortcode, $voucherId, $userId = null, $externalId = null, $externalReference = null, $purchaserMsisdn = null)
    {
        if(is_null($userId) && is_null($externalId))
        {
            throw new GenericException('ELS-Api: At least one of UserId/ExternalId is required for purchasing a voucher');
        }

        $url = "/ElP.Application.Voucher/Voucher/api/VoucherTransaction/PurchaseVoucher";
        $data = [
            'counterpartyId' => $partnerShortcode,
            'voucherInstances' => [
                ['voucherId' => $voucherId,],
            ],
            'userInfo' => [
                [
                    'userId' => $userId,
                    'externalId' => $externalId,
                    'counterpartyId' => null,
                ],
            ],
            'externalTransactionReference' => $externalReference,
            'purchaserMSISDN' => $purchaserMsisdn,
        ];

        $response = $this->post($url, $data);
        #$this->checkForErrors($response);

        return $response->json();
    }

    public function resendVoucher()
    {
        // ToDo
    }

    public function getVoucherHistory()
    {
        // ToDo
    }

    public function updateCodeStatus()
    {
        // ToDo
    }

    public function simulateRedeemVoucher()
    {
        // ToDo
    }

    public function redeemVoucher()
    {
        // ToDo
    }


    public function elsApi() : PendingRequest
    {
        return Http::retry(3, 100)
            ->bodyFormat('json')
            ->contentType('application/json-path+json')
            ->acceptJson()
            ->withHeaders([
                'PrivateKey' => $this->privateKey,
                'pid' => $this->programmeId
            ]);
    }

    protected function post($urlPart, $data)
    {
        Log::info('ELS-API: Sending post', ['url' => $urlPart, 'data' => $data]);
        $response = $this->elsApi()->post($this->host . $urlPart, $data);
        if($response->successful())
        {
            Log::debug('ELS-API: Got response from post', ['url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
        }
        else {
            Log::warning('ELS-API: Got invalid response from post', ['status' => $response->status(), 'url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
        }
        return $response;
    }

    protected function get($urlPart, $queryArray = [])
    {
        Log::info('ELS-API: Sending get', ['url' => $urlPart, 'data' => $queryArray,]);
        $response = $this->elsApi()->get($this->host . $urlPart, $queryArray);
        if($response->successful())
        {
            Log::debug('ELS-API: Got response from get', ['url' => $urlPart, 'data' => $queryArray, 'response-json' => $response->json()]);
        }
        else{
            Log::warning('ELS-API: Got invalid response from get', ['status' => $response->status(), 'url' => $urlPart, 'data' => $queryArray, 'response-json' => $response->json()]);
        }

        return $response;
    }


    protected function checkForErrors(Response $response)
    {
        if(!$response->json('isSuccessful'))
        {
            Log::error("ELS-Api: Unsuccessful call", ['response_json' => $response->json()]);
            throw new GenericException('ELS-Api: Fault detected');
        }
        else if($response->status() == 401)
        {
            throw new AuthorizationException($response);
        }
        else if($response->failed())
        {
            Log::error("ELS-Api: fault detected", ['response_json' => $response->json()]);
            throw new GenericException('ELS-Api: Fault detected');
        }
    }
}
