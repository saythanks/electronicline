<?php

namespace SayThanks\Electronicline;

use Illuminate\Support\ServiceProvider;

class ElectroniclineServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/electronicline.php', 'electronicline');
    }

    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/config/electronicline.php' => config_path('electronicline.php'),
            ], 'config');
    }


}